https://bookface.ycombinator.com/posts/2740

Ycombinator logo 7481412385fe6d0f7d4a3339d90fe12309432ca41983e8d350b232301d5d8684
Forum
Hours
Manual
Directory
Deals
S2012 
Scoutzie
jaigouk 

Search people or companies
Home
All Topics
Recent Comments
Guidelines
Post
FORUM POST
SEO Help for React Website (Single Page App)
We have a single page app that is generated exclusively on the client side. We would like Google to crawl the main page and any links to deeper content on that page. We had a few questions, because we've been reading that single page apps are often problematic with crawlers.

1) Is it important to render a more crawlable version on the backend for SEO or is a sitemap enough? Should we even do a sitemap? 2) Is it enough to only render title/meta-description on the backend? 3) Is it important to have an XML sitemap vs a TXT one? 4) Is the structure of the links itself important? Are hierarchies better than single long strings?

POSTED 2 DAYS AGO BY

Sumir Meghani
InstaworkS15
COMMENTS
0
https://github.com/mhart/react-server-example

Russell SmithRainforestS12about an hour ago
Reply
0
Hi Sumir, I previously used Prerender.io for an AngularJS single page application but it's basically the same mechanics:

Incoming requests from Slack, Facebook, Twitter, Google, Google Plus, etc, which have previews of the targeted content get routed to a scraped version of the URL on an s3 bucket. Prerenderio.io is just a service that makes it so it keeps it cached for a day so that, for example, if you created an article, it automatically gets scraped and pushed to s3, and you update the title, it needs to be scraped again so that Google's search results are up to date.

There's a few different middleware libs you can use but I used the nginx config with html5 push state (angular ui router).

1) If you put in the nginx config(https://prerender.io/documentation/install-middleware#nginx) and you're using html5 push state for your clean URLs, it should be fine. Not sure about the sitemap, it was more important for us that links can be shared on Facebook, Twitter, Slack with the content being correct...which benefits SEO too.

2) It's not enough for just the title/meta. The content (h1, main content) is part of evaluating whether the page is relevant to the search.

3) Not sure -- someone else with more experience with SEO can pitch in here

4) Not sure, as long as the link can be visited by a headless browser and it returns the proper http status code, it should be able to index that content

Hope that helps! Jaime Bueza

Jaime BuezaHealthSkoutW162 days ago
Reply

Type your comment here...
Keep me in the loop on this thread
Add Comment
GET NOTIFICATIONS

Follow to get emailed when people add comments.

Follow Forum Post
Privacy Policy Bookface Yes. I am the popular social networking site known as Bookface.