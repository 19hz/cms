### Imperative vs Declarative

declarative codes focuses on what we want do than how we want to do. `do something`.

<img src="/images/resources/AAA68FECCC97C77515A1F2F06D0F4E07.png" style="max-width: 90%">


### First React Component

```javascript
var React = require('react')
var ReactDOM = require('react-dom')
var HelloWorld = React.createClass({
  render: function(){
    return (
      <div>
        Hello World!
      </div>
    )
  }
});
ReactDOM.render(<HelloWorld />, document.getElementById('app'));
```

Signal to notify our app some data has changed→ Re-render virtual DOM -> Diff previous virtual DOM with new virtual DOM -> Only update real DOM with necessary changes

`npm run production`

### Nested Components and Props

Remember React is really good at managing state. Part of that is because there's a simple system for passing data from one component to another child component and that system is through props. props are to components what arguments are to functions.

```javascript
var FriendsContainer = React.createClass({
  render: function(){
    var name = 'Tyler McGinnis'
    var friends = ['Ean Platter', 'Murphy Randall', 'Merrick Christensen']
    return (
      <div>
        <h3> Name: {name} </h3>
        <ShowList names={friends} />
      </div>
    )
  }
});

var ShowList = React.createClass({
  render: function(){
    var listItems = this.props.names.map(function(friend){
      return <li> {friend} </li>;
    });
    return (
      <div>
        <h3> Friends </h3>
        <ul>
          {listItems}
        </ul>
      </div>
    )
  }
});
```

All map does is it creates a new array,

```javascript
var friends = ['Ean Platter', 'Murphy Randall', 'Merrick Christensen'];
var listItems = friends.map(function(friend){
  return "<li> " + friend + "</li>";
});
console.log(listItems);
// ["<li> Ean Platter </li>", "<li> Murphy Randall</li>", "<li> Merrick Christensen </li>]
```

### Building UIs with Pure Functions and Function Composition

 instead of your function taking in some arguments and returning a value,
 your function is going to take in some arguments and return some UI

 Function takes in some Data and returns a View

  instead of composing functions to get some value, we're composing functions to get some UI. This idea is so important in React that React 0.14 introduced Stateless Functional Components

```javascript
  var ProfilePic = function (props) {
   return <img src={'https://photo.fb.com/' + props.username} />
 }
 var ProfileLink = function (props) {
   return (
     <a href={'https://www.fb.com/' + props.username}>
       {props.username}
     </a>
   )
 }
 var Avatar = function (props) {
   return (
     <div>
       <ProfilePic username={props.username} />
       <ProfileLink username={props.username} />
     </div>
   )
 }
 <Avatar username="tylermcginnis" />
 ```

 One thing each of the functions and components above has in common is they're all "pure functions". The whole concept of a pure function is consistency and predictabilit

 consistency and predictability is because pure functions have the following characteristics.

- Pure functions always return the same result given the same arguments.
- Pure function's execution doesn't depend on the state of the application.
- Pure functions don't modify the variables outside of their scope.

React componets should be `FIRST`

* Focused
* Independent
* Resuable
* Small
* Testable

### Router

https://github.com/reactjs/react-router-tutorial

At its heart, React Router is a component.

we can not use 'class'.  we are in `JSX` land. it's a reserved word.
instead, we use `className`.

### Stateless Functional Components

separating your components into container components and presentational components, with presentational components optionally taking in some data and rendering a view.

Because this is such a common pattern in React, as of React 0.14 you can now have components that are just normal functions if those components only have a render method and optional props.

It's a good idea to try to use as many Stateless Functional Components as possible because then you have a good separation of presentational components vs other components.

Currently the biggest downfall with Stateless Functional Components are that they don't support shouldComponentUpdate

### PropTypes

PropTypes in React are the middle ground in terms of type checking properties that are passed to your components.

PropTypes are great for finding bugs in your components but what I like most about them is their ability to add documentation to a component.

To use PropTypes with functions the API is `propTypes.func` rather than propTypes.function. Also to use `booleans`, the API is `propTypes.bool` not propTypes.boolean. I'm not 100% sure why but I assume it's because with ES6 you can use named imports to do. both function and boolean are reserved words so that would break. Instead use func and bool and you'd be good.

```javascript
var React = require('react')
var PropTypes = React.PropTypes
var Icon = React.createClass({
  propTypes: {
    name: PropTypes.string.isRequired,
    size: PropTypes.number.isRequired,
    color: PropTypes.string.isRequired,
    style: PropTypes.object
  },
  render: ...
});
```

### this.props.children

```javascript
var Link = React.createClass({
  channgeURL: function() {
    window.location.replace({this.props.href})
  },
  render: function () {
    return (
      <span
        style={{color: 'blue', cursor: 'pointer'}}
        onClick={this.changeURL}>
        {this.props.children}
      </span>
    )
  }
})

var ProfileLink = React.createClass({
  render: function () {
    return (
      <div>
        <Link href={'https://www.github.com/' + this.props.username)>
          {this.props.username}
        </Link>
      </div>
    )
  }
})
```

### Life Cycle Events

 Lifecycle methods are special methods each component can have that allow us to hook into the views when specific conditions happen (i.e. when the component first renders or when the component gets updated with new data, etc).

two categories.


(1) When a component gets mounted to the DOM and unmounted.
(2) When a component receives new data.

** Mounting / Unmounting **

these methods will be invoked only once during the life of the component. mounting : the component is initialized and added to the DOM. unmounting : the component is removed from the DOM)

* getDefaultProps : we could make sure that if a text attribute isn't provided to the component, this.props.text will by default be 'Loading

```javascript
var Loading = React.createClass({
  getDefaultProps: function () {
    return {
      text: 'Loading'
    }
  },
  render: function () {
    ...
  }
})
```

* getInitialState : we've used getInitialState to set an email and password property on our state object in our Login component. To update the state, you can call this.setState passing in a new object which overwrites one or both of the email and password properties.

```javascript
var Login = React.createClass({
  getInitialState: function () {
    return {
      email: '',
      password: ''
    }
  },
  render: function () {
    ...
  }
})
```
* componentDidMount : This will get called right after the component is mounted to the DOM.

```javascript
var FriendsList = React.createClass({
  componentDidMount: function () {
    return Axios.get(this.props.url).then(this.props.callback)
  },
  render: function () {
    ...
  }
})
```

Set up any listeners (ie Websockets or Firebase listeners)

```javascript
var FriendsList = React.createClass({
  componentDidMount: function () {
    ref.on('value', function (snapshot) {
      this.setState({
        friends: snapshot.val()
      })
    })
  },
  render: function () {
    ...
  }
})
```

* componentWillUnmount : Now that we've set up that listener, we want to be sure to remove it when the component is removed from the DOM so we don't have memory leaks.

```javascript
var FriendsList = React.createClass({
  componentWillUnmount: function () {
    ref.off()
  },
  render: function () {
    ...
  }
})
```

* componentWillReceiveProps : Life Cycle Events that are going to be called whenever the component receives new data from its parent component.  There will be times that you'll want to execute some code whenever your component receives new props.

* shouldComponentUpdate : Life Cycle Events that are going to be called whenever the component receives new data from its parent component. returns a boolean, if that boolean is true, that component will re-render. If it's false, that component (and naturally all child components), won't re-render.


<img src="/images/resources/40AEDC487DE2F8EC6AAD471747E797B9.png" style="max-width: 90%">

react-lifecycle-parent-child.jsx

```javascript
import React from "react";
import { render } from "react-dom";

const ParentComponent = React.createClass({
  getDefaultProps: function() {
    console.log("ParentComponent - getDefaultProps");
  },
  getInitialState: function() {
    console.log("ParentComponent - getInitialState");
    return { text: "" };
  },
  componentWillMount: function() {
    console.log("ParentComponent - componentWillMount");
  },
  render: function() {
    console.log("ParentComponent - render");
    return (
      <div className="container">
        <input
          value={this.state.text}
          onChange={this.onInputChange} />
        <ChildComponent text={this.state.text} />
      </div>
    );
  },
  componentDidMount: function() {
    console.log("ParentComponent - componentDidMount");
  },
  componentWillUnmount: function() {
    console.log("ParentComponent - componentWillUnmount");
  },
  onInputChange: function(e) {
    this.setState({ text: e.target.value });
  }
});

const ChildComponent = React.createClass({
  getDefaultProps: function() {
    console.log("ChildComponent - getDefaultProps");
  },
  getInitialState: function() {
    console.log("ChildComponent - getInitialState");
    return { dummy: "" };
  },
  componentWillMount: function() {
    console.log("ChildComponent - componentWillMount");
  },
  componentDidMount: function() {
    console.log("ChildComponent - componentDidMount");
  },
  componentWillUnmount: function() {
    console.log("ChildComponent - componentWillUnmount");
  },
  componentWillReceiveProps: function(nextProps) {
    console.log("ChildComponent - componentWillReceiveProps");
    console.log(nextProps);
  },
  shouldComponentUpdate: function(nextProps, nextState) {
    console.log("ChildComponent - shouldComponentUpdate");
    return true;
  },
  componentWillUpdate: function(nextProps, nextState) {
    console.log("ChildComponent - componentWillUpdate");
    console.log("nextProps:");
    console.log(nextProps);
    console.log("nextState:");
    console.log(nextState);
  },
  render: function() {
    console.log("ChildComponent - render");
    return (
      <div>Props: {this.props.text}</div>
    );
  },
  componentDidUpdate: function(previousProps, previousState) {
    console.log("ChildComponent - componentDidUpdate");
    console.log("previousProps:");
    console.log(previousProps);
    console.log("previousState:");
    console.log(previousState);
  }
});

render(
  <ParentComponent />,
  document.getElementById("root")
);
```


### Axios, Promises, and the Github API

this keyword

- implicit binding
- explicit binding
- new binding
- window binding


#### this keyword

Implicit binding : left of the dot at call time.

```javascript
var Person = function(name, age) {
  return {
    name: name,
    age: age
    sayName: function() {
      console.log(this.name);
    },
    mother: {
      name: 'Stacey',
      sayName: function() {
        console.log(this.name);
      }
    }
  };
};

var jim = Person('Jim', 32);
jim.sayName(); // Jim
jim.mother.sayName(); // Stacey
```

Explicit binding : call, apply, bind

```javascript
var sayName = function() {
  console.log('My name is ' + this.name);
};

var stacey = {
  name: 'stacey',
  age: 34
};

sayName.call(stacey); // My name is stacey

var knowThings = function(lang1, lang2, lang3) {
  console.log('My name is ' + this.name +
    'and I know '+ lang1 + ', '+ lang2 + ', '+ lang3);
};

var languages = ['Javascript', 'Ruby', 'Elixir'];

// we are invoking knowThings in the context of stacey.
// and we are passing along 3 arguments.
// so it's explicit.
knowThings.call(stacey, languages[0], languages[1], languages[3]);

// by using apply, we can pass array of arguments
knowThings.apply(stacey, languges);

// .bind is almost same as .call except that it will return a new function
// instead of invoking the original function.
var newFn = knowThings.bind(stacey, languages[0], languages[1], languages[3]);
newFn();
```

New binding : object created by new keyword. inside of that obj, this mean the obj.

```javascript
var Animal = function(color, name, type) {
  // js will create this obj and use it
  // this = {}
  this.color = color;
  this.name = name;
  this.type = type;
};
var zebra = new Animal('stripes', 'zebra', 'horse');
```

Window binding: if there is no ref, this means window

```javascript
var sayAge = function() {
  console.log(this.age);
}
var me = {
  age: 24
}
sayAge(); //undefined
window.age = 34;
sageAge(); // 34
```

### More Container vs Presentational Components

```javascript
  var scores = [89, 76, 47, 95]
  var initialValue = 0
  var reducer = function (accumulator, item) {
    return accumulator + item
  }
  var total = scores.reduce(reducer, initialValue)
  var average = total / scores.length
```

You'll notice .reduce takes in two values, a callback function and an initial value.
 The thing that gets returned from the reducer function will then be passed as the accumulator the next time the function runs. .reduce can be used for more than transforming an array of numbers. It's all about that initialValue that you pass to reduce.

 ```javascript
 var votes = [
  'tacos',
  'pizza',
  'pizza',
  'tacos',
  'fries',
  'ice cream',
  'ice cream',
  'pizza'
]
var initialValue = {}
var reducer = function(tally, vote) {
  if (!tally[vote]) {
    tally[vote] = 1;
  } else {
    tally[vote] = tally[vote] + 1;
  }
  return tally;
}
var result = votes.reduce(reducer, initialValue) // {tacos: 2, pizza: 3, fries: 1, ice cream: 2}
```

**Array.map and Array.filter are both just types of Array.reduce?**

true.

[MDN Array.reduce](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce)

Sum all the values of an array

```javascript
var total = [0, 1, 2, 3].reduce(function(a, b) {
  return a + b;
});
// total == 6
```
Sum with initial value

```javascript
[0, 1, 2, 3, 4].reduce(function(previousValue, currentValue, currentIndex, array) {
  return previousValue + currentValue;
}, 10);
```

Flatten an array of arrays

```javascript
var flattened = [[0, 1], [2, 3], [4, 5]].reduce(function(a, b) {
  return a.concat(b);
}, []);
// flattened is [0, 1, 2, 3, 4, 5]
```
[MDN Array.prototype.filter](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter)

Filtering out all small values

```javascript
function isBigEnough(value) {
  return value >= 10;
}
var filtered = [12, 5, 8, 130, 44].filter(isBigEnough);
// filtered is [12, 130, 44]
```
Filtering invalid entries from JSON

```javascript
var arr = [
  { id: 15 },
  { id: -1 },
  { id: 0 },
  { id: 3 },
  { id: 12.2 },
  { },
  { id: null },
  { id: NaN },
  { id: 'undefined' }
];

var invalidEntries = 0;

function filterByID(obj) {
  if ('id' in obj && typeof(obj.id) === 'number' && !isNaN(obj.id)) {
    return true;
  } else {
    invalidEntries++;
    return false;
  }
}

var arrByID = arr.filter(filterByID);

console.log('Filtered Array\n', arrByID);
// Filtered Array
// [{ id: 15 }, { id: -1 }, { id: 0 }, { id: 3 }, { id: 12.2 }]

console.log('Number of Invalid Entries = ', invalidEntries);
// Number of Invalid Entries = 4
```

[MDN Array.protoype.map](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map)

Mapping an array of numbers to an array of square roots

```javascript
var numbers = [1, 4, 9];
var roots = numbers.map(Math.sqrt);
// roots is now [1, 2, 3], numbers is still [1, 4, 9]
```
Using map to reformat objects in an array

```javascript
var kvArray = [{key:1, value:10}, {key:2, value:20}, {key:3, value: 30}];
var reformattedArray = kvArray.map(function(obj){
   var rObj = {};
   rObj[obj.key] = obj.value;
   return rObj;
});
// reformattedArray is now [{1:10}, {2:20}, {3:30}],
// kvArray is still [{key:1, value:10}, {key:2, value:20}, {key:3, value: 30}]
```


Using map to reverse a string

```javascript
var str = '12345';
Array.prototype.map.call(str, function(x) {
  return x;
}).reverse().join('');

// Output: '54321'
// Bonus: use '===' to test if original string was a palindrome
```

Tricky use case

```javascript
// Consider:
['1', '2', '3'].map(parseInt);
// While one could expect [1, 2, 3]
// The actual result is [1, NaN, NaN]

// parseInt is often used with one argument, but takes two.
// The first is an expression and the second is the radix.
// To the callback function, Array.prototype.map passes 3 arguments:
// the element, the index, the array
// The third argument is ignored by parseInt, but not the second one,
// hence the possible confusion. See the blog post for more details

function returnInt(element) {
  return parseInt(element, 10);
}

['1', '2', '3'].map(returnInt); // [1, 2, 3]
// Actual result is an array of numbers (as expected)

// A simpler way to achieve the above, while avoiding the "gotcha":
['1', '2', '3'].map(Number); // [1, 2, 3]
```


### Private Components

 "private component" just as we would a private function.

```javascript
var React = require('react');
function FriendItem (props) {
  return <li>{props.friend}</li>
}
function FriendsList (props) {
  return (
    <h1>Friends:</h1>
    <ul>
      {props.friends.map((friend, index) => <FriendItem friend={friend} key={friend} />)}
    </ul>
  )
}
module.exports = FriendsList
```

 if you're using the "this" keyword, you're not 100% sure what the implementation of said function will look like. By removing the option to have a "this" keyword, we've removed the one way in which our function can be called in a way we're not expecting.

 A benefit of private functional stateless components are that they don't have a 'this' keyword


### Building a Highly Reusable React Component

getDefaultProps : getDefaultProps allows you to specify what the default props will be in a component if those specific props aren't specified when the component is invoked.

```javascript
var Loading = React.createClass({
  getDefaultProps: function () {
    return {
      text: 'loading',
      styles: {color: 'red'}
    }
  },
  render: function () {
    ...
  }
})
```

this.props.text will default to 'loading' and this.props.styles will default to {color: 'red'}. if our component is used like this, then this.props.text will be 'One second' and this.props.color will be 'green'.

```javascript
<Loading text='One second' styles={color: 'green'} />
```

### React Router Transition Animation and Webpack's CSS Loader

A React Element is "a plain object describing a component instance or DOM node and its desired properties".

```javascript
React.cloneElement(FriendList, {friends: ['Jake', 'Joe']})
```

```bash
$ npm install --save react-addons-css-transition-group
$ npm install --save css-loader style-loader
```

in webpack.config.js
```
  ...
  module: {
    loaders: [
      {test: /\.js$/, exclude: /node_modules/, loader: "babel-loader"},
      {test: /\.css$/, loader: "style-loader!css-loader"}
    ]
  },
  ...
```

Main.js

```javascript
var React = require('react');
var ReactCSSTransitionGroup = require('react-addons-css-transition-group')
require('../main.css')

/*
  we need to use React.cloneElement.
  because we need to attach key prop to childrent for ReactCSSTransitionGroup
  We cat not use {this.props.children key={xxx} }
 */
var Main  = React.createClass({
  render: function() {
    return (
      <div className='main-container'>
        <ReactCSSTransitionGroup
          transitionName="appear"
          transitionEnterTimeout={500}
          transitionLeaveTimeout={500}>
            {React.cloneElement(this.props.children, {key: this.props.location.pathname})}
        </ReactCSSTransitionGroup>
      </div>
    )
  }
});

module.exports = Main;
```
