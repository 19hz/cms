https://www.udemy.com/react-redux/

```javascript
const myArray = [1,2,3]
console.log(myArray.concat([5,6,7]))
// [1, 2, 3, 5, 6, 7]

// ES6 syntax for concat array
console.log([...myArray, 5, 6, 7])
```

using lodash for sum

```javascript
import _ from 'lodash'
function average (data) {
  // return(data.reduce((prev, curr) => prev + curr) / data.length)
  return(_.round(_.sum(data)/data.length))
}
```

destructuring

```javascript
const lon = cityData.city.coord.lon
const lat = cityData.city.coord.lat

// same as this
const {lon, lat} = cityData.city.coord
```
![redux.png](resources/E34C1355C0FB42761C1E6F0D5191BD9E.png)

![redux-promise.png](resources/99A90702E166C3306DBBFF5ED0241E93.png)

![redux-middleware.png](resources/B82A995077E1AE69E2C3574EA7FBC92A.png)