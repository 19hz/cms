#  ES6 for React

http://courses.reactjsprogram.com/courses/es6forreact

## Babel. const. let. Modules. Destructuring.

 TC-39 committee. Each new feature starts out at Stage 0 and progresses to Stage 4. Stage 0 being experimental, Stage 4 being finalized and ready for release in the next version

```bash
 npm install --save-dev babel-preset-es2015.
 ```

 .bablerc file
```javascript
{
  "presets": [
    "react",
    "babel-preset-es2015"
  ]
}
```

## New Variable Declarations with let and const

only functions introduce new scopes.

Now the reason I'm putting so much emphasis on scope is because that's the biggest differentiator between var and let. let allows you to have code which is block scoped, meaning anywhere we have an opening and closing curly brace we're creating a new scope

```javascript
function doThing() {
  var num = 1;
  if (num >= 0) {
    let secondNum = 2;
    console.log(num); // 1
    console.log(secondNum); // 2
  }
  console.log(num); // 1
  console.log(secondNum); // Uncaught ReferenceError: secondNum is not defined
}
doThing();
```

Now that brings us to const. Everything let has, const also has. The only difference is that when you create a variable with const, that variable can't be re-assigned a new reference. Notice I didn't say that variable is immutable.

```javascript
const user = {
  name: 'Tyler',
  age: 25
}

user.name = 'Joey'
```

The code above is valid. We're not re-assigning the reference to user, we're just re-assigning a specific value.

```javascript
const user = {
  name: 'Tyler',
  age: 25
}
user = {
  name: 'Joey', 
  age: 25
}
```

Now this will throw a "user is already defined" error since we're trying to re-assign the actual reference.

I always default with const, if I'm mutating the variable, I'll use let. Rarely do I ever use var. Even though const isn't purely immutable, whenever I see a const variable I treat it as such.

##  import and export with ES6 Module

```javascript
// math.js
export function add (x,y) {
  return x + y
}
export function multiply (x,y) {
  return x * y
}
export function divide (x,y) {
  return x / y
}
// main.js
import { add, multiply } from './math'
add(1,2) // 3
multiply(3,4) // 12
```
you can use `import * as X`

```javascript
// math.js (same as above)
// main.js
import * as math from './math'
math.add(1,2) // 3
math.multiply(3,4) // 12
math.divide(4,4) // 1
```
You can also have modules that only export single values using default. *Note here we're not using named imports.

```javascript
// math.js
export default function doAllTheMath (x,y,z) {
  return x + y + x * x * y * z / x / y / z
}
// main.js
import doAllTheMath from './math'
doAllTheMath(1,2,3) // 4
```

You can even mix named exports with default exports

```javascript
// math.js
export function add (x,y) {
  return x + y
}
export default function doAllTheMath (x,y,z) {
  return x + y + x * x * y * z / x / y / z
}
// main.js
import doAllTheMath, { add } from './math'
doAllTheMath(1,2,3) // 4
add(1,2) // 3
```

## Object Destructuring

using `{ }` to extract values (or methods) from data stored in objects and arrays.

```javascript
function register (props) {
  var { onChangeEmail, email, onChangePassword, password, submit }  = props;
  return (
    <div>
      <span>Email:</span>
      <input type='text' onChange={onChangeEmail} value={email} />
      <span>Password:</span>
      <input type='text' onChange={onChangePassword} value={password} />
      <button onClick={submit}>Submit</button>
    </div>
  )
}
```

requireing 

```javascript
var ReactRouter = require('react-router');
var Route = ReactRouter.Route;
var Link = ReactRouter.Link;
var Router = ReactRouter.Router;
```
Now using named imports, the code above cleans up to look like this.
```javascript
var { Route, Link, Router } = require('react-router')
```

## Concise Object Methods

"Concise Object Methods" allow you to drop the ": function" part of a method.

```javascript
var actions = {
  sayName: function () {
    alert(this.name)
  },
  takeStep: function () {
    this.step++
  }
}
```

Now we can write ^ code as,

```javascript
var actions = {
  sayName () {
    alert(this.name)
  },
  takeStep () {
    this.step++
  }
}
```

## Arrow Functions

 more concise syntax and sharing lexical "this" with the parent scope.

now you can write functions like this. DO YOU REMEMBER CONFFEESCRIPT?

```javascript
const doThing = (thing) => {

}
// OR
function doThing () {

}
```
keeping this context into a nested function is cumbersome. you need to call `.bind(this) without ES6.

```javascript
var FriendsList = React.createClass({
  getInitialState () {
    return {
      friends: [
        {id: 0, name: 'Mikenzi'},
        {id: 1, name: 'Ryan'},
        {id: 2, name: 'Jake'},
      ]
    }
  },
  onAddFriend (friend) {
    this.setState({
      friends: this.state.friends.concat([friend])
    })
  }
  render () {
    return (
      <ul>
        {this.state.friends.map(function (friend) {
          return <FriendItem key={friend.id} handleAddFriend={this.onAddFriend}>{friend.name}</FriendItem>
        }.bind(this))} />
    )
  }
});
```

Because we've created another function, we're now in a different context. One way to fix this is to add .bind(this) to the end of the function in our map invocation. Another more elegant way though is to use an arrow function instead. The reason for this is because arrow functions don't create a new context, so the "this" keyword inside map will be the same as the "this" keyword outside of map, which solves our problem.

```javascript
render () {
    return (
      <ul>
        {this.state.friends.map((friend) => {
          return <FriendItem key={friend.id} handleAddFriend={this.onAddFriend}>{friend.name}></FriendItem>
        })}
      </ul>
    )
  }
```

Another benefit of using arrow functions is that, if you have everything on one line, the arrow function will implicitly return whatever is on that line. Here's what I mean by that. Our render method below has the same functionality as the one above, but no return statement.

```javascript
  render () {
    return (
      <ul>
        {this.state.friends.map((friend) => <FriendItem key={friend.id} handleAddFriend={this.onAddFriend}>{friend.name}</FriendItem>)}
      </ul>
    )
  }
```


## Template Strings

```javascript
function makeGreeting (name, email, id) {
  return `Hello, ${name}. We've emailed you at ${email}. Your user id is ${id}.`
}
```
supports mulit-line strings

```javascript
var thing = `this is a multi
 line
 update and it is 
 valid.
`
```

## Default Parameters

From this 

```javascript
function debounce (func, wait, immediate) {
  if (typeof wait === 'undefined') {
    wait = 1000
  }
  var timeout
  return function () {
    var args = arguments
    var later = () => {
      timeout = null
      if (!immediate) func.apply(this, args)
    }
    var callNow = immediate && !timeout
    clearTimeout(timeout)
    timeout = setTimeout(later, wait)
    if (callNow) func.apply(this, args)
  }
}
```

To this
```javascript
function debounce (func, wait = 1000, immediate) {
  var timeout
  return function () {
    var args = arguments
    var later = () => {
      timeout = null
      if (!immediate) func.apply(this, args)
    }
    var callNow = immediate && !timeout
    clearTimeout(timeout)
    timeout = setTimeout(later, wait)
    if (callNow) func.apply(this, args)
  }
}
```

## Concise Objects

From this

```javascript
function getUser (username) {
  const email = getEmail(username)
  return {
    username: username,
    email: email
  }
}
```

to this

```javascript
function getUser (username) {
  const email = getEmail(username)
  return {
    username,
    email
  }
}
```

## arguemnts

we can remove props 

```javascript
function ConfirmBattle (props) {
  return (
    props.isLoading === true
    ? <Loading />
    : <MainContainer>
        <h1>Confirm Players </h1>
        <div className='col-sm-8 co-sm-offset-2'>
            <UserDetailsWrapper header='Player One'>
              <UserDetails info={props.playersInfo[0]} />
            </UserDetailsWrapper>
            <UserDetailsWrapper header='Player One'>
              <UserDetails info={props.playersInfo[1]} />
            </UserDetailsWrapper>
        </div>
        <div className='col-sm-8 co-sm-offset-2'>
          <div className='col-sm-12' style={space}>
            <button type='button' className='btn btn-lg btn-success' onClick={props.onInitiateBattle}>
              Initiate ConfirmBattle
            </button>
          </div>
          <div className='col-sm-12' style={space}>
            <Link to='/playerOne'>
              <button type='button' className='btn btn-lg btn-danger'>
                Reselect Players
              </button>
            </Link>
          </div>
        </div>
    </MainContainer>
  )
}
```

following code alerts Tyler

```javascript
 function sayName (user) { 
   alert(user.name) 
 } 
 
 const name = 'Tyler' 
 sayName({name}) 
```

## Async/Await

```
npm install --save-dev babel-preset-stage-3 babel-polyfill
```
you can't use await without using async. That's why it's called async/await. The rule I like to repeat to myself is this, "if I want to use await, I need to make sure that the function I'm in is an async function." What that looks like in code is this.

```javascript
async function handleGetUser () {
  try {
    var user = await getUser()
    console.log(user)
  } catch (error) {
    console.log('Error in handleGetUser', error)
  }
}
````
We've successfully gotten rid of our .then method and replaced it with more synchronous looking code.



Read [MDN Promise doc](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Promise), ["Understanding Promises before you use Async Await"](https://medium.com/@bluepnume/learn-about-promises-before-you-start-using-async-await-eb148164a9c8#.qpt8mtimv) and ["We have a problem with promises"](https://pouchdb.com/2015/05/18/we-have-a-problem-with-promises.html)
[Fluent Conf 2016 Talk video: Jeremy Fairbank - The rise of async JavaScript](https://www.youtube.com/watch?v=QtgR94Q2pt4)

A Promise represents a proxy for a value not necessarily known when the promise is created. It allows you to associate handlers to an asynchronous action's eventual success value or failure reason. This lets asynchronous methods return values like synchronous methods: instead of the final value, the asynchronous method returns a promise of having a value at some point in the future.

A Promise is in one of these states:

* pending: initial state, not fulfilled or rejected.
* fulfilled: meaning that the operation completed successfully.
* rejected: meaning that the operation failed.

 A promise is said to be `settled` if it is either `fulfilled` or `rejected`, but not `pending`. You will also hear the term "resolved" used with promises — this means that the promise is `settled`, or it is "locked" into a promise chain. 
 
 ![promises.png](https://mdn.mozillademos.org/files/8633/promises.png)
 
- `Promise.all(iterable)` : Returns a promise that either resolves when all of the promises in the iterable argument have resolved or rejects as soon as one of the promises in the iterable argument rejects. If the returned promise resolves, it is resolved with an array of the values from the resolved promises in the iterable. If the returned promise rejects, it is rejected with the reason from the promise in the iterable that rejected. This method can be useful for aggregating results of multiple promises together.
- `Promise.race(iterable)` : Returns a promise that resolves or rejects as soon as one of the promises in the iterable resolves or rejects, with the value or reason from that promise.
- `Promise.reject(reason)` : Returns a Promise object that is rejected with the given reason.
- `Promise.resolve(value)` : Returns a Promise object that is resolved with the given value. If the value is a thenable (i.e. has a then method), the returned promise will "follow" that thenable, adopting its eventual state; otherwise the returned promise will be fulfilled with the value. Generally, if you want to know if a value is a promise or not - Promise.resolve(value) it instead and work with the return value as a promise.

> every async function you write will return a promise, and every single thing you await will ordinarily be a promise.

- Pitfall 1: not awaiting
- Pitfall 2: awaiting multiple values
- Pitfall 3: your whole stack needs to be async
- Pitfall 4: Gotta remember to handle errors

A promise is a special kind of javascript object which contains another object. I could have a promise for the integer 17, or the string “hello world”, or some arbitrary object, or anything else you could normally store in a javascript variable.

How do I access the data in a promise? I use `.then()`. How do I catch the errors from a promise chain? I use `.catch()`

```javascript
function getFirstUser() {
    return getUsers().then(function(users) {
        return users[0].name;
    }).catch(function(err) {
        return {
          name: 'default user'
        };
    });
}
```

 Any promise we have, using ES2016, we can await. That’s literally all await means: it functions in exactly the same way as calling `.then()` on a promise (but without requiring any callback function). So the above code becomes
 
```javascript
async function getFirstUser() {
    let users = await getUsers();
    return users[0].name;
}
```

it’s important to remember: async functions don’t magically wait for themselves. You must await, or you’ll get a promise instead of the value you expect.

under normal usage, I can only await one thing at a time

```javascript
let [foo, bar] = await Promise.all([getFoo(), getBar()]);
```
 The reason this works because async/await and promises are the same thing under the hood. Promise.all will take an array of promises, and compose them all into a single promise, which resolves only when every child promise in the array has resolved itself.
 
 
  Promise.all means “Wait for these things” not “Do these things”. It is not equivalent to async.parallel which actually calls the methods you pass into it.
  
  
```javascript
function callbackToPromise(method, ...args) {
    return new Promise(function(resolve, reject) {
        return method(...args, function(err, result) {
            return err ? reject(err) : resolve(result);
        });
    });
}
```


then 

```javascript
async function getFirstUser() {
    let users = await callbackToPromise(getUsers);
    return users[0].name;
}
```

Unless `myApp.endpoint` (which is a function you probably don’t own) is promise/async aware, and is calling `await` or `.catch()` on anything returned from the handler function I pass in, any errors are going to be lost.


```javascript
myApp.endpoint('GET', '/api/firstUser', async function(req, res) {
    let firstUser = await getFirstUser();
    res.json(firstUser)
});
```

the top level of your code always needs to be enclosed in a try/catch (gotta catch ’em all) to make sure you’re handling any errors:

```javascript
myApp.registerEndpoint('GET', '/api/firstUser', async function(req, res) {
    try {
        let firstUser = await getFirstUser();
        res.json(firstUser)
    } catch (err) {
        console.error(err);
        res.status(500);
    }
})
```



** Q: What is the difference between these four promises? **

```javascript
doSomething().then(function () {
  return doSomethingElse();
});

doSomething().then(function () {
  doSomethingElse();
});

doSomething().then(doSomethingElse());

doSomething().then(doSomethingElse);
```

 as exposed in modern browsers as window.Promise. Not all browsers have window.Promise though.
 
 ** Rookie mistake #1: the promisey pyramid of doom **
 ```javascript
 remotedb.allDocs(...).then(function (resultOfAllDocs) {
  return localdb.put(...);
}).then(function (resultOfPut) {
  return localdb.get(...);
}).then(function (resultOfGet) {
  return localdb.put(...);
}).catch(function (err) {
  console.log(err);
});
```

** Rookie mistake #2: WTF, how do I use forEach() with promises? **

forEach()/for/while are not the constructs you're looking for. You want Promise.all():

```javascript
db.allDocs({include_docs: true}).then(function (result) {
  return Promise.all(result.rows.map(function (row) {
    return db.remove(row.doc);
  }));
}).then(function (arrayOfResults) {
  // All docs have really been removed() now!
});
```

** Rookie mistake #3: forgetting to add .catch() **

Unfortunately this means that any thrown errors will be swallowed, and you won't even see them in your console. This can be a real pain to debug.

```javascript
somePromise().then(function () {
  return anotherPromise();
}).then(function () {
  return yetAnotherPromise();
}).catch(console.log.bind(console)); // <-- this is badass
```

** Rookie mistake #4: using "deferred" **

strategy is to use the revealing constructor pattern, which is useful for wrapping non-promise APIs. For instance, to wrap a callback-based API like Node's fs.readFile(), you can simply do

```javascript
new Promise(function (resolve, reject) {
  fs.readFile('myfile.txt', function (err, file) {
    if (err) {
      return reject(err);
    }
    resolve(file);
  });
}).then(/* ... */)
```

** Rookie mistake #5: using side effects instead of returning **

the magic of promises is that they give us back our precious return and throw. But what does this actually look like in practice?

Every promise gives you a then() method (or catch(), which is just sugar for then(null, ...)). Here we are inside of a then() function:

```javascript
somePromise().then(function () {
  // I'm inside a then() function!
});
```

What can we do here? There are three things:

- return another promise
- return a synchronous value (or undefined)
- throw a synchronous error

(1) Return another promise

This is a common pattern you see in the promise literature, as in the "composing promises" example above:

```javascript
getUserByName('nolan').then(function (user) {
  return getUserAccountById(user.id);
}).then(function (userAccount) {
  // I got a user account!
});
```

Notice that I'm returning the second promise – that return is crucial. If I didn't say return, then the getUserAccountById() would actually be a side effect, and the next function would receive undefined instead of the userAccount.


(2) Return a synchronous value (or undefined)

Returning undefined is often a mistake, but returning a synchronous value is actually an awesome way to convert synchronous code into promisey code. For instance, let's say we have an in-memory cache of users. We can do,

```javascript
getUserByName('nolan').then(function (user) {
  if (inMemoryCache[user.id]) {
    return inMemoryCache[user.id];    // returning a synchronous value!
  }
  return getUserAccountById(user.id); // returning a promise!
}).then(function (userAccount) {
  // I got a user account!
});
```

Unfortunately, there's the inconvenient fact that non-returning functions in JavaScript technically return undefined, which means it's easy to accidentally introduce side effects when you meant to return something.

For this reason, I make it a personal habit to always return or throw from inside a then() function. I'd recommend you do the same.

(3) Throw a synchronous error

Speaking of throw, this is where promises can get even more awesome. Let's say we want to throw a synchronous error in case the user is logged out. It's quite easy:

```javascript
getUserByName('nolan').then(function (user) {
  if (user.isLoggedOut()) {
    throw new Error('user logged out!'); // throwing a synchronous error!
  }
  if (inMemoryCache[user.id]) {
    return inMemoryCache[user.id];       // returning a synchronous value!
  }
  return getUserAccountById(user.id);    // returning a promise!
}).then(function (userAccount) {
  // I got a user account!
}).catch(function (err) {
  // Boo, I got an error!
});
```

catch() will receive a synchronous error if the user is logged out, and it will receive an asynchronous error if any of the promises are rejected. Again, the function doesn't care whether the error it gets is synchronous or asynchronous.


** Advanced mistake #1: not knowing about Promise.resolve() **

As I showed above, promises are very useful for wrapping synchronous code as asynchronous code. However, if you find yourself typing this a lot:

```javascript
new Promise(function (resolve, reject) {
  resolve(someSynchronousValue);
}).then(/* ... */);
```

You can express this more succinctly using Promise.resolve():

```javascript
Promise.resolve(someSynchronousValue).then(/* ... */);
```

> Just remember: any code that might `throw` synchronously is a good candidate for a nearly-impossible-to-debug swallowed error somewhere down the line. But if you wrap everything in `Promise.resolve()`, then you can always be sure to `catch()` it later.

```javascript
function somePromiseAPI() {
  return Promise.resolve().then(function () {
    doSomethingThatMayThrow();
    return 'foo';
  }).then(/* ... */);
}
```

Similarly, there is a Promise.reject() that you can use to return a promise that is immediately rejected:

```javascript
Promise.reject(new Error('some awful error'));
```

** Advanced mistake #2: catch() isn't exactly like then(null, ...) **

t catch() is just sugar. when you use the `then(resolveHandler, rejectHandler)` format, the `rejectHandler` won't actually catch an error if it's thrown by the `resolveHandler` itself.

```javascript
somePromise().then(function () {
  throw new Error('oh noes');
}).catch(function (err) {
  // I caught your error! :)
});

somePromise().then(function () {
  throw new Error('oh noes');
}, function (err) {
  // I didn't catch your error! :(
});
```
For this reason, I've made it a personal habit to never use the second argument to `then()`, and to always prefer catch(). The exception is when I'm writing asynchronous Mocha tests, where I might write a test to ensure that an error is thrown:

```javascript
it('should throw an error', function () {
  return doSomethingThatThrows().then(function () {
    throw new Error('I expected an error!');
  }, function (err) {
    should.exist(err);
  });
});
```

** Advanced mistake #3: promises vs promise factories **

`Promise.all()` doesn't execute the promises in parallel.

```javascript
function executeSequentially(promises) {
  var result = Promise.resolve();
  promises.forEach(function (promise) {
    result = result.then(promise);
  });
  return result;
}
```
 this will not work the way you intended. The promises you pass in to executeSequentially() will still execute in parallel. you don't want to operate over an array of promises at all. Per the promise spec, as soon as a promise is created, it begins executing. So what you really want is an array of promise factories:

```javascript
function executeSequentially(promiseFactories) {
  var result = Promise.resolve();
  promiseFactories.forEach(function (promiseFactory) {
    result = result.then(promiseFactory);
  });
  return result;
}
```

A promise factory is very simple, though – it's just a function that returns a promise. ? It works because a promise factory doesn't create the promise until it's asked to. It works the same way as a then function – in fact, it's the same thing!

```javascript
function myPromiseFactory() {
  return somethingThatCreatesAPromise();
}
```

If you look at the `executeSequentially()` function above, and then imagine `myPromiseFactory` being substituted inside of `result.then(...)`, then hopefully a light bulb will click in your brain. At that moment, you will have achieved promise enlightenment.

** Advanced mistake #4: okay, what if I want the result of two promises?**

As your promise code starts to get more complex, you may find yourself extracting more and more functions into named functions. I find this leads to very aesthetically-pleasing code, which might look like this:

```javascript
putYourRightFootIn()
  .then(putYourRightFootOut)
  .then(putYourRightFootIn)  
  .then(shakeItAllAbout);
```

** Advanced mistake #5: promises fall through **

What do you think this code prints out?

```javascript
Promise.resolve('foo').then(Promise.resolve('bar')).then(function (result) {
  console.log(result);
});
```

If you think it prints out bar, you're mistaken. It actually prints out foo! The reason this happens is because when you pass `then()` a non-function (such as a promise), it actually interprets it as `then(null)`, which causes the previous promise's result to fall through. You can test this yourself:

```javascript
Promise.resolve('foo').then(null).then(function (result) {
  console.log(result);
});
```

> you can pass a promise directly into a then() method, but it won't do what you think it's doing. then() is supposed to take a function

```javascript
Promise.resolve('foo').then(function () {
  return Promise.resolve('bar');
}).then(function (result) {
  console.log(result);
});
```
> ============================================================
>
> So just remind yourself: always pass a function into then()!
>
> ============================================================

for these examples, I’m assuming that both doSomething() and doSomethingElse() return promises, and that those promises represent something done outside of the JavaScript event loop (e.g. IndexedDB, network, setTimeout)

Puzzle #1

```javascript
doSomething().then(function () {
  return doSomethingElse();
}).then(finalHandler);
```
Answer:
```
doSomething
|-----------------|
                  doSomethingElse(undefined)
                  |------------------|
                                     finalHandler(resultOfDoSomethingElse)
                                     |------------------|
```

Puzzle #2

```javascript
doSomething().then(function () {
  doSomethingElse();
}).then(finalHandler);
```
Answer:
```
doSomething
|-----------------|
                  doSomethingElse(undefined)
                  |------------------|
                  finalHandler(undefined)
                  |------------------|
```

Puzzle #3

```javascript
doSomething().then(doSomethingElse())
  .then(finalHandler);
```
Answer:
```
doSomething
|-----------------|
doSomethingElse(undefined)
|---------------------------------|
                  finalHandler(resultOfDoSomething)
                  |------------------|
```

Puzzle #4
```javascript
doSomething().then(doSomethingElse)
  .then(finalHandler);
```
Answer:
```
doSomething
|-----------------|
                  doSomethingElse(resultOfDoSomething)
                  |------------------|
                                     finalHandler(resultOfDoSomethingElse)
                                     |------------------|
```



[Bluebird 3.0](http://bluebirdjs.com/docs/features.html) will print out warnings that can prevent many of the mistakes 

> tyler-mcginnis [10:57 PM] Bluebird is a really popular promise library. However, ES6 comes with its own promise implementation, so I don't use bluebird or q anymore.


## Classes

When you use createClass you need to be sure to call functions in the right context, with Classes, React autobinds "this" for you? NOPE.


From this
```javascript
var TodoApp = React.createClass({ 
  propTypes: {
    title: PropTypes.string.isRequired
  },
  getInitialState () { 
    return {
      items: []
    }; 
  },
  updateItems (newItem) { 
    var allItems = this.state.items.concat([newItem]); 
    this.setState({items: allItems}); 
  }, 
  render () { 
    return ( 
      <div> 
        <TodoBanner title={this.props.title}/> 
        <TodoList items={this.state.items}/> 
        <TodoForm onFormSubmit={this.updateItems}/> 
      </div> 
    ); 
  } 
});
```
to this
```javascript
class TodoApp extends React.Component {
  constructor () {
    super()
    this.state = {
      items: []
    }
  }
  updateItems (newItem) { 
    var allItems = this.state.items.concat([newItem]); 
    this.setState({items: allItems}); 
  } 
  render () { 
    return ( 
      <div> 
        <TodoBanner title={this.props.title}/> 
        <TodoList items={this.state.items}/> 
        <TodoForm onFormSubmit={() => this.updateItems()}/> 
      </div> 
    ); 
  } 
}
TodoApp.propTypes = {
  title: PropTypes.string.isRequired
}
```

*  This essentially replaces our need to getInitialState since we can set a state instance on the component itself during the constructor phase. Also note before you set anything to the instance you need to call super() which calls the constructor function of the thing we're extending
* We no longer have commas between our properties since we're not in an object anymore.
* PropTypes are moved out of the class and added afterwards, just like we're used to doing with Stateless Functional Components.
* `this` keyword would NOT BOUND TO THE ORIGITAL CONTEXT. We can use .bind to fix this problem when we're passing in a function to another component (context) or we can use our handy dandy arrow functions as we did passing updateItems to onFormSubmit.

```javascript
<TodoForm onFormSubmit={this.updateItems}/> 
```

```javascript
<TodoForm onFormSubmit={() => this.updateItems()}/> 
```

[MDN Doc for Classes](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Classes)

Prototype methods

methods with The get syntax binds an object property to a function that will be called when that property is looked up.

```javascript
class Polygon {
  constructor(height, width) {
    this.height = height;
    this.width = width;
  }
  
  get area() {
    return this.calcArea();
  }

  calcArea() {
    return this.height * this.width;
  }
}

const square = new Polygon(10, 10);

console.log(square.area);
```

Static methods are called without instantiating their class and are also not callable when the class is instantiated. Static methods are often used to create utility functions for an application.

```javascript
class Point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    static distance(a, b) {
        const dx = a.x - b.x;
        const dy = a.y - b.y;

        return Math.sqrt(dx*dx + dy*dy);
    }
}

const p1 = new Point(5, 5);
const p2 = new Point(10, 10);

console.log(Point.distance(p1, p2));
```

Sub classing with extends

```javascript
class Cat { 
  constructor(name) {
    this.name = name;
  }
  
  speak() {
    console.log(this.name + ' makes a noise.');
  }
}

class Lion extends Cat {
  speak() {
    super.speak();
    console.log(this.name + ' roars.');
  }
}
```

Mix-ins

```javascript
var CalculatorMixin = Base => class extends Base {
  calc() { }
};

var RandomizerMixin = Base => class extends Base {
  randomize() { }
};
```

A class that uses these mix-ins can then be written like this:

```javascript
class Foo { }
class Bar extends CalculatorMixin(RandomizerMixin(Foo)) { }
```