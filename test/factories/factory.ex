# https://github.com/thoughtbot/ex_machina
defmodule Cms.Factory do
  # with Ecto
  use ExMachina.Ecto, repo: Cms.Repo
  def factory(:user) do
    %Cms.User{
      first_name: "Jane",
      last_name: "Smith",
      email: sequence(:email, &"email-#{&1}@example.com"),
      password: "password1234",
      encrypted_password: "encypptedeeee"
    }
  end
end
