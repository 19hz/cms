defmodule Cms.PageControllerTest do
  use Cms.ConnCase

  test "GET /", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "Loading"
  end
end
