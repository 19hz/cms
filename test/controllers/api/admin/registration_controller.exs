defmodule Cms.RegistrationControllerTest do
  use Cms.ConnCase

  @valid_attrs %{first_name: 'jay', email: 'jay@exmaple.com', password: 'password1234'}
  @invalid_attrs %{}

  test "/api/admin/registrations with no param" do
    conn = post conn, "/"
    body = json_response(conn, 200)
    assert "can't be blank" in body["errors"]
  end

  test "/api/admin/registrations  with param" do
    conn = post conn, "/", user: @invalid_attrs
    body = json_response(conn, 404)
    assert fail
    # assert "asdfasdfaf" in body["jwt"]
    # assert "jddddy" in body["user"]["first_name"]
    # assert "kim" in body["user"]["last_name"]
    # assert "jay@exmaple.com" in body["user"]["email"]
  end

end
