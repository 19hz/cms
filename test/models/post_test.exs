defmodule Cms.PostTest do
  use Cms.ModelCase, async: true
  alias Cms.{Post}
  import Cms.Factory


  @valid_attrs %{title: "some content", slug: "test-elixir"}
  @invalid_attrs %{}

  setup do
    {:ok, user: create(:user)}
  end

  test "changeset with valid attributes", %{user: user} do
    attributes = @valid_attrs
      |> Map.put(:user_id, user.id)

    changeset = Post.changeset(%Post{}, attributes)

    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Post.changeset(%Post{}, @invalid_attrs)
    refute changeset.valid?
  end
end
