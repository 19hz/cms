import test from 'ava'
import mkdirp from 'mkdirp'
import { LocalStorage } from 'node-localstorage'
import { Api } from '../../../../web/static/js/utils/api'
import { Constants } from '../../../../web/static/js/constants'

class TestApi extends Api {
  constructor(storage) {
    super(storage)
    this.defaultHeader = {
      Origin: 'http://localhost:4000',
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }
  }
}

test.before(() => {
  mkdirp('../fixture')
})


test('login:Unauthorized', async (t) => {
  const storage = new LocalStorage('../fixture/api_login_unauthorized')
  const api = new TestApi(storage)
  try {
    await api.login('admin@test.com', 'pass')
    t.is(res.status, 401)
    // storage.clear()
  } catch (error) {
    t.pass(error)
  }
})

test('login:Authorized', async t => {
  try {
    const storage = new LocalStorage('../fixture/api_login_authorized')
    const api = new TestApi(storage)
    await api.login('admin@test.com', 'password')
    t.is(res.status, 200)
    console.log(res.body)
    // storage.clear()
  } catch (error) {
    t.pass(error)
  }
})

test('logout', async t => {
  const storage = new LocalStorage('../fixture/api_login_logout')
  const api = new TestApi(storage)
  api.login('admin@test.com', 'password').then(function stepOne() {
    return api.logout()
  }).then(function stepTwo(res) {
    t.is(res.status, 204)
    // storage.clear()
  }).catch(function wrong(error) {
    t.pass(error)
  })
})
