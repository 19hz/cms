require('babel-register')
require('babel-polyfill')
require('supertest-as-promised')

var serialize = require('serialize-javascript')

global.document = require('jsdom').jsdom('<body></body>')
global.window = document.defaultView
global.navigator = window.navigator

// global.__API_URL__ = 'https://gogovan-dev.herokuapp.com/api/w1/'
var envVars = require('../../../config/test')
global.ENV = envVars
