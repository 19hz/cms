# CMS

Git based blog + cms.

- contents are in github repo with markdown files
- categories are github repo
- admin can arrange contents
- uploading css (bootstrap theme), images

## Stack

  * React.js / Redux / [GraphQL(absinthe)](http://absinthe-graphql.org)
  * Webpack (module bundler)
  * [MDL(Material Design Lite)](http://www.getmdl.io)
  * Phoenix (live-reload)
    * Cowboy (web server)
    * BEAM (VM)

## Get started

  * Install Phoenix dependencies with `mix deps.get`
  * Install node modules `npm install`
  * Start Phoenix endpoint with `mix phoenix.server`
  * after updating `pakcage.json`, lock down versions by running `npm shrinkwrap`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

## Admin

```bash
> iex -S mix
> alias Cms.Repo
> alias Cms.User
> params = %{name: "Jaigouk Kim",username: "jaigouk", password: "jk5425"}
> changeset = User.registration_changeset(%User{}, params)
> Repo.insert(changeset)
```

## Linting

  * [Configure Text Editors](https://github.com/kriasoft/react-starter-kit/blob/master/docs/how-to-configure-text-editors.md)

## debugging

  * IEx.pry : use `IEx.pry` for debugging. Here is [a tutorial](http://blog.plataformatec.com.br/2016/04/debugging-techniques-in-elixir-lang) from plataformatec.

```elixir
require IEx;
  defmodule Cms.PostController do
    ...
    def index(conn, _params) do
      current_user = Guardian.Plug.current_resource(conn)
      posts = current_user
        |> assoc(:posts)
        |> Repo.all
        |> Repo.preload(:user)
  IEx.pry      
      render conn, "index.json", posts: posts
    end
    ...
  end
end
```

And fireup phoenix with iex.

```bash
> iex -S mix phoenix.server
```

You can start debugger

```bash
:debugger.start()
```

## Testing

  * Install [ava snippets](https://github.com/sindresorhus/ava#related) for your editor
  * Write a test file as `xx.test.js` and use [AVA assertions](https://github.com/sindresorhus/ava#assertions) or [redux-ava](https://github.com/sotojuan/redux-ava)
  * `npm test` or `npm run test:watch`

## Style Guides

  * [JavaScript](https://github.com/airbnb/javascript)
  * [React](https://github.com/airbnb/javascript/tree/master/react)

## logo credits CC

```html
Miu Icons graphic by <a href="http://linhpham.me/miu">Linh Pham</a> from <a href="http://www.flaticon.com/">Flaticon</a> is licensed under <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0">CC BY 3.0</a>. Made with <a href="http://logomakr.com" title="Logo Maker">Logo Maker</a>
```
