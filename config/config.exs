# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# Configures the endpoint
config :cms, Cms.Endpoint,
  url: [host: "localhost"],
  root: Path.dirname(__DIR__),
  secret_key_base: "C0/kk5Z2HSg7h+dX4SKeXdQlkN5+eO/zAllV9Dy5ZhRBomt8V/7YibdWTB/LjSrM",
  render_errors: [accepts: ~w(html json)],
  pubsub: [name: Cms.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :guardian, Guardian,
  allowed_algos: ["HS512"], # optional
  verify_module: Guardian.JWT,  # optional
  issuer: "Cms",
  ttl: { 30, :days },
  verify_issuer: true, # optional
  secret_key: "A0/kk5Z2HSg7h4dX4SKeXdQlkNE+eO/zAllV9Dy5ZhRBoXt8V/7YibdWTB5LjSrC",
  serializer: Cms.GuardianSerializer

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"

# Configure phoenix generators
config :phoenix, :generators,
  migration: true,
  binary_id: false
