# require IEx;
defmodule Cms.PostController do
  use Cms.Web, :controller

  alias Cms.{Repo, Post, User}

  plug Guardian.Plug.EnsureAuthenticated, handler: Cms.SessionController

  plug :scrub_params, "post" when action in [:create, :update]

  def index(conn, _params) do
    current_user = Guardian.Plug.current_resource(conn)
    posts = current_user
      |> assoc(:posts)
      |> Repo.all
      |> Repo.preload(:user)
# IEx.pry      
    render conn, "index.json", posts: posts
  end

  def show(conn, %{"id" => id}) do
    post = Repo.get(Cms.Post, id)
      |> Repo.preload(:user)
    render conn, "show.json", post: post
  end


  def create(conn, %{"post" => post_params}) do
    current_user = Guardian.Plug.current_resource(conn)

    changeset = current_user
      |> build_assoc(:posts)
      |> Post.changeset(post_params)

    if changeset.valid?  do
      post = Repo.insert!(changeset)
        |> Repo.preload(:user)

      conn
      |> put_status(:created)
      |> render("show.json", post: post )
    else
      conn
      |> put_status(:unprocessable_entity)
      |> render("error.json", changeset: changeset)
    end
  end

  def update(conn, %{"id" => id, "post" => post_params}) do
    post = Repo.get!(Cms.Post, id)
    changeset = Post.changeset(post, post_params)

    case Repo.update(changeset) do
      {:ok, post} ->
        conn
        |> put_status(:updated)
        |> render("show.json", post: post )
      {:error, changeset} ->
        render(conn, "edit.json", post: post, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    post = Repo.get!(Cms.Post, id)
    Repo.delete!(post)
    conn
    |> render("delete.json")
  end
end
