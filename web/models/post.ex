defmodule Cms.Post do
  use Cms.Web, :model

  @derive {Poison.Encoder, only: [:id, :title, :slug, :user]}

  schema "posts" do
    field :uuid, Ecto.UUID
    field :title, :string
    field :slug, :string
    field :html
    field :markdown
    field :image, :string
    field :meta_title, :string
    field :meta_description, :string
    field :status, :string
    field :page, :boolean
    field :published_at, Ecto.DateTime
    belongs_to :user, Cms.User
    timestamps
  end

  @required_fields ~w(title slug user_id)
  @optional_fields ~w(markdown html image meta_title meta_description status page status)


  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end

  def update_changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end
end
