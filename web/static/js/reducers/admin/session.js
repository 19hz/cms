import Constants from 'cms_web/constants'

const initialState = {
  currentUser: null,
  loading: null,
  error: null,
}

function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case Constants.CURRENT_USER:
      return { ...state, currentUser: action.currentUser, loading: null, error: null }

    case Constants.SESSIONS_LOADING:
      return { ...state, loading: true, error: null }

    case Constants.SESSIONS_ERROR:
      return { ...state, loading: null, error: action.error }

    case Constants.USER_LOGGED_OUT:
      return initialState

    default:
      return state
  }
}

export default reducer
