import { combineReducers } from 'redux'
import { routeReducer }     from 'redux-simple-router'

const initialState = {
  currentUser: null,
  loading: null,
  error: null,
}

function starter(state = initialState, action = {}) {
  return state
}

export default combineReducers({
  routing: routeReducer,
  starter: starter,
})
