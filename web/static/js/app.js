import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import { Router, browserHistory } from 'react-router'

import reducers from './reducers/app'
import routes from './routes/app'

const createStoreWithMiddelwere = applyMiddleware()(createStore)

ReactDOM.render(
  <Provider store={createStoreWithMiddelwere(reducers)}>
    <Router history={browserHistory} routes={routes}/>
  </Provider>
  ,document.querySelector('#root'))
