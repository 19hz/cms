import axios from 'axios'
import { Constants } from '../../../../web/static/js/constants'

export class Api {
  constructor(storage = window.localStorage) {
    this.defaultHeader = {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }
    this.store = storage
    const base = `#{ENV.API_URL}/api/admin/`
    this.axios = axios.create({
      baseURL: base
    })
  }

  loggedIn() {
    return !!this.store.getItem('authToken')
  }

  clearToken() {
    this.store.removeItem('authToken')
  }

  async login(email, password) {
    try {
      const data = {
        session: {
          email,
          password,
        },
      }

      const info = await this.request('post', 'login', data)
      const token = info.jwt
      this.store.setItem('authToken', token)
      // console.log(info)
      return info
    } catch (error) {
      // console.warn('Error in loginUser', error)
      throw error
    }
  }

  async logout() {
    try {
      const data = {}
      const res = await this.request('delete', 'logout', data)
      return res
    } catch (error) {
      throw error
    } finally {
      this.clearToken()
    }
  }

  request(method, path, data) {
    try {
      if (path !== 'login') {
        const authToken = this.store.getItem('authToken')
        this.defaultHeader.Authorization = `${authToken}`
      }
      const config = {
        method,
        url: path,
        data,
        headers: this.defaultHeader,
      }
      return this.axios.request(config)
    } catch (error) {
      throw error
    }
  }
}

const api = new Api(window.localStorage)
export default api
