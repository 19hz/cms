import { push } from 'react-router-redux'

import Constants from 'cms_web/constants'
import api from 'cms_web/utils/api'
// import NotificationActions from './notification'

export function setCurrentUser(dispatch, user) {
  dispatch({
    type: Constants.CURRENT_USER,
    currentUser: user,
  })

  // dispatch(NotificationActions.subscribeNotification(user.id))
}

const Actions = {
  login: (email, password) => {
    return dispatch => {
      dispatch({
        type: Constants.SESSIONS_LOADING
      })

      api.login(email, password)
      .then((res) => {
        setCurrentUser(dispatch, res)
        dispatch(push('/'))
      })
      .catch((error) => {
        dispatch({
          type: Constants.SESSIONS_ERROR,
          error: error.statusText,
        })
      })
    }
  },

  logout: () => {
    return dispatch => {
      api.logout()
      .then(() => {
        dispatch({ type: Constants.USER_LOGGED_OUT })
        dispatch(push('/login'))
      })
      .catch((error) => {
        // console.log(error)
      })
    }
  },

  currentUser: () => {
    return dispatch => {
      api.request('get', '/current_user')
      .then((res) => {
        setCurrentUser(dispatch, res)
      })
      .catch((error) => {
        // console.log(error)
        api.clearToken()
        dispatch(push('/login'))
      })
    }
  },
}

export default Actions
