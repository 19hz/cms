import React from 'react'
import {  Route, IndexRoute } from 'react-router'
import Admin from '../../components/admin/component-admin-app'
import AdminLogin from '../../components/admin/component-admin-login'

const Greeting = () => {
  return <div> Hey there</div>
}

export default (
  <Route path='/admin' component={Admin}>
    <IndexRoute component={AdminLogin} />
    <Route path="greet" component={Greeting} />
    <Route path="greet2" component={Greeting} />
    <Route path="greet3" component={Greeting} />
  </Route>
)

// import { IndexRoute, IndexRedirect, Redirect, Route } from 'react-router'
// import Actions from 'cms_web/actions/admin/session'
// import api from 'cms_web/utils/api'
//
// import MainLayout from 'cms_web/views/main'
// import AuthenticatedContainer from 'cms_web/containers/authenticated'
//
// import UsersLoginView from 'cms_web/views/users/login'
// export default function configRoutes(store) {
//   const _checkAuthenticated = (nextState, replace, callback) => {
//     const { dispatch } = store
//     const { session } = store.getState()
//     const { currentUser } = session
//
//     if (api.loggedIn()) {
//       if (!currentUser) {
//         dispatch(Actions.currentUser())
//       }
//       switch (nextState.location.pathname) {
//         case '/login':
//           replace('/')
//           break
//         case '/logout':
//           dispatch(Actions.logout())
//           break
//       }
//     } else {
//       if (nextState.location.pathname !== '/login') {
//         replace('/login')
//       }
//     }
//
//     callback()
//   }
//
//   return (
//     <Route component={MainLayout}>
//       <Route path="/login" component={UsersLoginView} onEnter={_checkAuthenticated} />
//       <Route path="/logout" onEnter={_checkAuthenticated} />
//
//       <Route path="/" component={AuthenticatedContainer} onEnter={_checkAuthenticated}>
//         <IndexRedirect to="/orders" />
//         <Route path="orders">
//           <IndexRoute component={OrdersView} />
//           <Redirect path="view" to="/orders" />
//
//           <Route path="new">
//             <IndexRoute component={OrdersNewView} />
//             <Route path="setting" component={OrdersNewExtraView} />
//             <Route path="pickup" component={OrdersNewPickupView} />
//             <Route path="destination" component={OrdersNewDestinationView} />
//           </Route>
//         </Route>
//
//       </Route>
//     </Route>
//   )
// }
