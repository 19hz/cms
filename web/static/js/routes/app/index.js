import React from 'react'
import { Route, IndexRoute } from 'react-router'
import ReactMarkdown from 'react-markdown'
import App from '../../components/app/component-app'

import PostsIndex from '../../components/app/component-app-posts-index'


const md_react = react_doc
const md_es6 = es6_doc
const md_redux = redux_doc
const md_concept = react_concept
const md_thunk = redux_thunk_doc

const MdReact = () => {
  return(
    <div>
      <ReactMarkdown source={md_react} />
    </div>
  )
}

const MdEs6 = () => {
  return(
    <div>
      <ReactMarkdown source={md_es6} />
    </div>
  )
}

const MdRedux = () => {
  return(
    <div>
      <ReactMarkdown source={md_redux} />
    </div>
  )
}

const MdConcept = () => {
  return(
    <div>
      <ReactMarkdown source={md_concept} />
    </div>
  )
}

const MdThunk = () => {
  return(
    <div>
      <ReactMarkdown source={md_thunk} />
    </div>
  )
}


export default (
  <Route path='/' component={App}>
    // <IndexRoute component={PostsIndex} />
    <IndexRoute component={MdConcept} />
    <Route path="react" component={MdReact} />
    <Route path="es6" component={MdEs6} />
    <Route path="redux" component={MdRedux} />
    <Route path="react_concept" component={MdConcept} />
    <Route path="redux-thunk" component={MdThunk} />
  </Route>
)
