import React, { PropTypes } from 'react'
import { Card, CardTitle, CardText,CardActions,
         Textfield, CardMenu, Button, IconButton } from 'react-mdl'

class AdminLogin extends React.Component {
  render () {
    return(
      <div>
        <Card shadow={0} style={{width: '512px', margin: 'auto'}}>
          <CardTitle style={{color: '#fff', height: '176px', background: 'url(http://www.getmdl.io/assets/demos/welcome_card.jpg) center / cover'}}>Welcome</CardTitle>
          <CardText style={{display: 'flex',  alignItems: 'center'}}>
            <Textfield
              onChange={() => {}}
              label="email for login"
              style={{width: '200px'}}
            />
            <Textfield
              onChange={() => {}}
              label="Password"
              type="password"
              style={{width: '200px'}}
            />
          </CardText>
        <CardActions
          border
          style={{display: 'flex',  alignItems: 'center'}}
        >
          <Button colored>Login</Button>
        </CardActions>
        <CardMenu style={{color: '#fff'}}>
          <IconButton name="share" />
        </CardMenu>
      </Card>
      </div>
    )
  }
}

export default AdminLogin
