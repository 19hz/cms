import { Layout, Header, Drawer,  Navigation, FABButton, Button, Icon, IconButton, Card, CardText, CardTitle,  CardActions, CardMenu, Content } from 'react-mdl'
import React, { PropTypes } from 'react'

class Admin extends React.Component {
  render () {
    return(
      <div className="admin-layout mdl-layout mdl-js-layout">
        <Layout fixedHeader fixedDrawer>
          <Header title={<span><span style={{ color: '#ddd' }}>Area / </span><strong>The Title</strong></span>}>
            <Navigation>
                <a href="">Link</a>
                <a href="">Link</a>
                <a href="">Link</a>
                <a href="">Link</a>
            </Navigation>
          </Header>
          <Drawer title="Title">
            <Navigation>
                <a href="">Link</a>
                <a href="">Link</a>
                <a href="">Link</a>
                <a href="">Link</a>
            </Navigation>
          </Drawer>
          <Content className="admin-main">

            <h3>Admin</h3>
            <FABButton colored ripple>
              <Icon name="add" />
            </FABButton>
            <Button raised accent ripple>Button</Button>
            
            {this.props.children}
          </Content>
        </Layout>
      </div>
    )
  }
}

export default Admin
