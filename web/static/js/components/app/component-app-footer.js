import React, { PropTypes } from 'react'
import { Footer, FooterSection, FooterLinkList } from 'react-mdl'

class AppFooter extends React.Component {
  render () {
    return(
      <div>
        <Footer size="mini" className="app-footer">
          <FooterSection type="left">
            <FooterLinkList>
              <a href="#">Help</a>
              <a href="#">Privacy and Terms</a>
              <a href="#">User Agreement</a>
            </FooterLinkList>
          </FooterSection>
        </Footer>
      </div>
    )
  }
}

export default AppFooter
