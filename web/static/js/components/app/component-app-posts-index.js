import React, { PropTypes } from 'react'
import { Switch } from 'react-mdl'
class PostsIndex extends React.Component {
  render () {
    return(
      <div>
        Posts Index
        <Switch ripple id="switch1" defaultChecked>Ripple switch</Switch>

        <Switch id="switch2">Switch</Switch>
      </div>
    )
  }
}

export default PostsIndex;
