import classNames from 'classnames'
import React, { PropTypes } from 'react'
import { Layout, Header, Navigation, Drawer, Textfield, Content, Grid, Cell,
         Footer, FooterSection, FooterLinkList, Switch } from 'react-mdl'
import DocumentTitle from 'react-document-title'
import AppFooter from './component-app-footer'

const title = 'Learning React'

function getColorClass(color, level) {
  const lvlClass = (level) ? `-${level}` : ''
  return `mdl-color--${color}${lvlClass}`
}

function getTextColorClass(color, level) {
    const lvlClass = (level) ? `-${level}` : '';
    return `mdl-color-text--${color}${lvlClass}`;
}

class App extends React.Component {
  render () {
    return(
      <div>
        <DocumentTitle title={title}>
          <Layout
             className={classNames('app-layout', getColorClass('grey', 100))}
             fixedHeader
          >
          <Header
            title={<span><span style={{ color: '#ddd' }}>19Hz / </span><strong>{title}</strong></span>}
          >
            <Navigation>
              <a href="/react">React</a>
              <a href="/es6">ES6</a>
              <a href="/redux">Redux</a>
              <a href="/redux-thunk">Redux Thunk</a>
              <a href="/react_concept">React Concept</a>
            </Navigation>
          </Header>
          <Drawer title="Learning React">
              <Navigation>
                  <a href="/react">React</a>
                  <a href="/es6">ES6</a>
                  <a href="/redux">Redux</a>
                  <a href="/redux-thunk">Redux Thunk</a>
                  <a href="/react_concept">React Concept</a>
              </Navigation>
          </Drawer>
            <div className="app-ribbon" />
            <Content className="app-main">
              <Grid className="app-container">
                <Cell col={2} hidePhone hideTablet />
                <Cell col={8} shadow={2} className={classNames('app-content', getColorClass('white'), getTextColorClass('grey', 800))}>
                  {this.props.children}
                </Cell>
              </Grid>
            </Content>
            <AppFooter />
          </Layout>
        </DocumentTitle>
      </div>
    )
  }
}

export default App
