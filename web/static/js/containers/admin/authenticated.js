import { Component } from 'react'
import { connect } from 'react-redux'
import { routeActions } from 'react-router-redux'
import Header from 'b2b_web/views/header'

class AuthenticatedContainer extends Component {
  render() {
    const { currentUser, dispatch, socket } = this.props

    if (!currentUser) return false

    return (
      <div className="application-container">
        <Header />

        <div className="main-container">
          {this.props.children}
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  currentUser: state.session.currentUser,
  socket: state.session.socket,
  channel: state.session.channel,
})

export default connect(mapStateToProps)(AuthenticatedContainer)
