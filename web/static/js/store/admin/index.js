import { createStore, applyMiddleware } from 'redux'
import createLogger                     from 'redux-logger'
import thunkMiddleware                  from 'redux-thunk'
import { Router, Route, browserHistory } from 'react-router'
import { syncHistoryWithStore, routerReducer } from 'react-router-redux'
import reducers                         from '../../reducers/admin'

const loggerMiddleware = createLogger({
  level: 'info',
  collapsed: true,
})

// Add the reducer to your store on the `routing` key
const store = createStore(
  combineReducers({
    ...reducers,
    routing: routerReducer
  })

  // Create an enhanced history that syncs navigation events with the store
const history = syncHistoryWithStore(browserHistory, store)
