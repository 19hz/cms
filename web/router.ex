defmodule Cms.Router do
  use Cms.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :browser_session do
    plug Guardian.Plug.VerifySession
    plug Guardian.Plug.LoadResource
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug Guardian.Plug.VerifyHeader
    plug Guardian.Plug.LoadResource
  end

  pipeline :graphql do
    plug Cms.Context
  end

  scope "/api", Cms do
    pipe_through :api
    scope "/admin" do
      post "/registrations", RegistrationController, :create
      get "/current_user", CurrentUserController, :show
      post "/login", SessionController, :create, as: :login
      delete "/logout", SessionController, :delete, as: :logout
      resources "posts", PostController, only: [:index, :show, :update, :create, :delete]
    end
  end

  # http://www.phoenixframework.org/docs/routing
  scope "/admin", as: :admin do
    pipe_through [:browser, :browser_session]
    get "*path", Cms.Admin.AdminController, :index
  end

  scope "/", Cms do
    pipe_through [:browser]
    get "*path", PageController, :index
  end

  scope "/api" do
    pipe_through :graphql
    forward "/", Absinthe.Plug, schema: Cms.Schema
  end

end
