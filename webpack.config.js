// https://github.com/webpack/docs/wiki/optimization
// https://shellmonger.com/2016/02/26/splitting-vendor-and-app-javascript-files-with-webpack/
var path = require('path')
var fs = require('fs')
var webpack = require('webpack')
var ExtractTextPlugin = require('extract-text-webpack-plugin')
var CopyWebpackPlugin = require('copy-webpack-plugin')
// var CommonsChunkPlugin = require('webpack/lib/optimize/CommonsChunkPlugin')

// helpers for writing path names
// e.g. join("web/static") => "/full/disk/path/to/hello/web/static"
function join(dest) { return path.resolve(__dirname, dest) }
function web(dest) { return join('web/static/' + dest) }
function nodeModules(dest) { return join('node_modules/' + dest) }

var serialize = require('serialize-javascript')

var reactDoc = fs.readFileSync('./data/a.reactjsprogram.com.md').toString()
var es6Doc = fs.readFileSync('./data/b.ES6forReact.md').toString()
var reduxDoc = fs.readFileSync('./data/c.ReduxCourse.md').toString()
var reactConcept = fs.readFileSync('./data/react-concept.md').toString()
var reduxThunk = fs.readFileSync('./data/redux-thunk.md').toString()

var config = module.exports = {
  devtool: "source-map",

  // our application's entry points - for this example we'll use a single each for
  // css and js
  entry: {
    app: [
      'babel-polyfill',
      web('css/app.scss'),
      web('js/app.js') ],
    admin: [
      'babel-polyfill',
      web('css/admin.scss'),
      web('js/admin.js') ]
  },

  // where webpack should output our files
  output: {
    path: join('priv/static'),
    filename: 'js/[name].js'
  },

  resolve: {
    alias: {
      phoenix_html: join('deps/phoenix_html/web/static/js/phoenix_html'),
      phoenix: join("deps/phoenix/web/static/js/phoenix.js"),
      sinon: nodeModules('sinon/pkg/sinon'),
      cms_web: web("js")
    }
  },

  // more information on how our modules are structured, and
  //
  // in this case, we'll define our loaders for JavaScript and CSS.
  // we use regexes to tell Webpack what files require special treatment, and
  // what patterns to exclude.
  module: {
    loaders: [
      {
        test: /sinon\.js$/,
        loader: 'imports?define=>false,require=>false'
      },
      {
        test: /\.js$/,
        exclude: /node_modules|bower_components/,
        loader: "babel-loader",
        query: {
          cacheDirectory: true,
          plugins: ['transform-decorators-legacy'],
          presets: ["react", "babel-preset-es2015", "babel-preset-stage-0"]
        }
      },
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract(
          'style',
          'css!sass'
        )
      },
      { test: /\.css$/,
        loader: 'style-loader!css-loader'
      },
      {
        test: /\.json$/,
        loader: 'json'
      },
    ]
  },

  // what plugins we'll be using - in this case, just our ExtractTextPlugin.
  // we'll also tell the plugin where the final CSS file should be generated
  // (relative to config.output.path)

  plugins: [
    new webpack.DefinePlugin({
      ENV: serialize(require("./config/" + (process.env.NODE_ENV || "development"))),
      react_doc: JSON.stringify(reactDoc),
      es6_doc: JSON.stringify(es6Doc),
      redux_doc: JSON.stringify(reduxDoc),
      react_concept: JSON.stringify(reactConcept),
      redux_thunk_doc: JSON.stringify(reduxThunk)
    }),
    // new webpack.optimize.CommonsChunkPlugin(/* chunkName= */"vendor", /* filename= */"vendor.bundle.js"),
    new webpack.ProvidePlugin({
      'React': 'react',
      'ReactDOM': 'react-dom',
      'classnames': 'classnames',
      'PageClick': 'react-page-click',
      'sinon': 'sinon',
      'sinonChai': 'sinon-chai',

      'moment': 'moment',
      '_': 'lodash',
    }),
    new ExtractTextPlugin("css/[name].css"),
    new CopyWebpackPlugin([
      { from: web("assets") },
      { from: web("vendor/js"), to: 'js'},
      { from: web("vendor/css"), to: 'css'},
      { from: "./deps/phoenix_html/web/static/js/phoenix_html.js",
        to: "js/phoenix_html.js" }
    ])
  ],

  // these are for karma to work properly
  externals: {
    "jsdom": "window",
    'react/lib/ExecutionEnvironment': true,
    'react/lib/ReactContext': 'window',
    'text-encoding': 'window'
  },
}

// if running webpack in production mode, minify files with uglifyjs
if (process.env.NODE_ENV === 'production') {
  config.plugins.push(
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.UglifyJsPlugin({ minimize: true })
  )
}
