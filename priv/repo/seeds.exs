alias Cms.{Repo, User}

[
  %{
    first_name: "John",
    last_name: "Doe",
    email: "admin@exaple.com",
    password: "password"
  },
]
|> Enum.map(&User.changeset(%User{}, &1))
|> Enum.each(&Repo.insert!(&1))
