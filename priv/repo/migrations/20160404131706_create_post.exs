defmodule Cms.Repo.Migrations.CreatePost do
  use Ecto.Migration
    def change do
      create table(:posts) do
        add :uuid, :string
        add :title, :string, size: 100, null: false
        add :slug, :string, size: 100, null: false
        add :html, :text
        add :markdown, :text
        add :image, :string
        add :meta_title, :string
        add :meta_description, :string
        add :author_id, :integer
        add :status, :string
        add :page, :boolean
        add :published_at, :datetime
        add :user_id, references(:users, on_delete: :delete_all), null: false
        timestamps
      end
    create unique_index(:posts, [:slug])
    create unique_index(:posts, [:uuid])
  end
end
